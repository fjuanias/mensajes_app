/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.app.mensajes_app;

import java.util.Scanner;

/**
 *
 * @author fhjua
 */
public class MensajesService {
    
    public static void crearMensaje() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Escribe tu mensaje:");
        String mensaje = sc.nextLine();
        
        System.out.println("Escribe tu nombre:");
        String autor = sc.nextLine();
        
        Mensajes registro = new Mensajes();
        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(autor);
        
        MensajesDAO.crearMensajeDb(registro);
    }
    
    public static void listarMensajes() {
        MensajesDAO.leerMensajesDb();
    }
    
    public static void borrarMensaje() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("ID del mensaje:");
        int id = sc.nextInt();
        
        MensajesDAO.borrarMensajesDb(id);
    }
    
    public static void editarMensaje() {
        Scanner sc = new Scanner(System.in);
        
        int id;
        String mensaje, autor;
        Mensajes registro = new Mensajes();
        
        System.out.println("Indica el ID a modificar:");
        id = sc.nextInt();
        
        sc.nextLine();
        
        System.out.println("Indica el nuevo mensaje:");
        mensaje = sc.nextLine();
        
        System.out.println("Indica el nuevo autor:");
        autor = sc.nextLine();
        
        registro.setId_mensaje(id);
        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(autor);
        
        MensajesDAO.actualizarMensajeDb(registro);
    }
    
}
