/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.app.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author fhjua
 */
public class MensajesDAO {
    
    public static void crearMensajeDb(Mensajes mensaje) {
        Conexion db = new Conexion();
        try (Connection conexion = db.getConexion()) {
            PreparedStatement ps = null;
            try {
               String query = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?,?)";
               ps = conexion.prepareStatement(query);
               ps.setString(1, mensaje.getMensaje());
               ps.setString(2, mensaje.getAutor_mensaje());
               ps.executeUpdate();
               System.out.println("Mensaje creado exitosamente!");
            } catch (SQLException e) {
                System.out.println(e);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public static void leerMensajesDb() {
        Conexion db = new Conexion();
        try (Connection conexion = db.getConexion()) {
            PreparedStatement ps = null;
            ResultSet rs = null;
            
            String query = "SELECT * FROM mensajes";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                System.out.println("Id: " + rs.getInt("id_mensaje"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha: " + rs.getString("fecha_mensaje"));
                System.out.println("");
            }
        } catch (SQLException e) {
            System.out.println("Error al obtener los datos");
            System.out.println(e);
        }
    }
    
    public static void borrarMensajesDb(int id_mensaje) {
        Conexion db = new Conexion();
        try (Connection conexion = db.getConexion()) {
            PreparedStatement ps = null;
            try {
                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensaje);
                ps.executeUpdate();
                System.out.println("Mensaje borrado exitosamente!");
            } catch (SQLException e) {
                System.out.println("Error al borrar el mensaje");
                System.out.println(e);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public static void actualizarMensajeDb(Mensajes mensaje) {
        Conexion db = new Conexion();
        try (Connection conexion = db.getConexion()) {
            PreparedStatement ps = null;
            try {
                String query = "UPDATE mensajes SET mensaje = ?, autor_mensaje = ? WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.setInt(3, mensaje.getId_mensaje());
                ps.executeUpdate();
                System.out.println("Mensaje actualizado exitosamente!");
            } catch (SQLException e) {
                System.out.println("Error al actualizar el mensaje");
                System.out.println(e);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
}
