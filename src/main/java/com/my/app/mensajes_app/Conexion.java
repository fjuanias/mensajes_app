/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my.app.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author fhjua
 */
public class Conexion {
    public Connection getConexion() {
        Connection conexion = null;
        try {
            conexion = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/mensajes_app?useSSL=false&serverTimezone=America/Bogota&allowPublicKeyRetrieval=true","root","");
        } catch(SQLException ex) {
            System.out.println(ex);
        }
        return conexion;
    }
}
